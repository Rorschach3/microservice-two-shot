from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "vo_id"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model",
        "manufacturer",
        "color",
        "picture_url",
        "id",
        "bin"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


class BinListEncoder(ModelEncoder):
    model = BinVO
    properties = ["vo_id",
                  "name"]


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            # Get the BinVO vo_id and put it in the content dict
            bin_object = BinVO.objects.get(vo_id=content["bin"])
            content['bin'] = bin_object

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin vo_id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )

    else:
        if request.method == "GET":
            shoe = Shoe.objects.all()
            return JsonResponse(
                {"shoes": shoe},
                encoder=ShoeListEncoder,
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeListEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Shoe does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                {"message": "The shoe was deleted"}
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist"})
    else:  # PUT

        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=pk)

            # Get the BinVO vo_id and put it in the content dict
            bin_object = BinVO.objects.get(vo_id=content["bin"])
            content['bin'] = bin_object
            props = ["model", "manufacturer", "color", "picture_url", "bin"]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeListEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Shoe not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def api_list_binvo(request):
    if request.method == "GET":
        bins = BinVO.objects.all()
        return JsonResponse(
            {"bins": bins},
            encoder=BinVODetailEncoder,
        )
