from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    vo_id = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = "BinVO"
        verbose_name_plural = "BinVOs"


class Shoe(models.Model):
    model = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.model} - {self.manufacturer}/{self.color}"

    class Meta:
        ordering = ("model", "manufacturer", "color")
        verbose_name = "Shoe"
        verbose_name_plural = "Shoes"
