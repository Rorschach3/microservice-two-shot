# Generated by Django 4.0.3 on 2023-06-02 22:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BinVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vo_id', models.PositiveSmallIntegerField()),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'BinVO',
                'verbose_name_plural': 'BinVOs',
            },
        ),
        migrations.CreateModel(
            name='Shoe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model', models.CharField(max_length=100)),
                ('manufacturer', models.CharField(max_length=100)),
                ('color', models.CharField(max_length=100)),
                ('picture_url', models.URLField(null=True)),
                ('bin', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='shoes', to='shoes_rest.binvo')),
            ],
            options={
                'verbose_name': 'Shoe',
                'verbose_name_plural': 'Shoes',
                'ordering': ('model', 'manufacturer', 'color'),
            },
        ),
    ]
