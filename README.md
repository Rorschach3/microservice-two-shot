Team:

* [Daniel Hernandez](https://www.gitlab.com/Rorschach3)- Hats microservice
* [Dalton Carl](https://www.gitlab.com/DaltonAC) - Shoes microservice

## Design

## Shoes microservice

The approach I took to creating and implemetning the shoes microsoervice was to allow the creation and deletion of shoes. A way for the user to display the shoes they have created and attach to a Bin location. They should also be able to remove shoes from the list through the front-end interface. Giving them a easy option to add new ones as well. The shoes microserver does not interact or interfer with the Hats models/views.

### Shoes Models

```python
class Shoe(models.Model):
    model = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
    )
```

This shoe model allows for the creation of shoes. With a ForeignKey of BinVO which pulls in the location of the shoe when it was created. The BinVO model is a value object that relates to the Bin model inside of the wardrone API. This pulls the required information from the Bin model to attach to Shoes.

```python
class BinVO(models.Model):
    vo_id = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = "BinVO"
        verbose_name_plural = "BinVOs"
```

The BinVO model works with the poller function to get informtion through the wardrobe APU using a restful method to implement the API calls. At this time the BinVO only gets the name of the closest as that is the only required information currently on the front-end.

### Shoes Views

```python
@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            # Get the BinVO vo_id and put it in the content dict
            bin_object = BinVO.objects.get(vo_id=content["bin"])
            content['bin'] = bin_object

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin vo_id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )

    else:
        if request.method == "GET":
            shoe = Shoe.objects.all()
            return JsonResponse(
                {"shoes": shoe},
                encoder=ShoeListEncoder,
            )
```

This view lists the shoes created and allows teh creation of new shoes when a "POST" request is sent. Using JSON a request to GET or POST information is handled through the local server. When trying to make a new shoe and it is not assigned a Bin or given the wrong BinVO Id it will return message to the user. This prevents shoes from being created that can not be assessible on the front end that require a location to be valid to be shown through my react component.

```python
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model",
        "manufacturer",
        "color",
        "picture_url",
        "id",
        "bin"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }
```

The encoder created to accompany the shoe list takes in the Shoe model and returns all the properties that are required for the front-end creation of the shoes. The addtional encoder within it is is to retreive the properties from the BinO model.

## Hats microservice

<<<<<<< HEAD
Hats microservice is a website for creating and listing hats as part of a wardrobe service. Hats has 2 pages associated with the micrservice `HatsForm` and `HatsList` They can both be accessd from the main page listed @`http://localhost:3000/`. The form page for hats allows you to create a new hat based on the properties of "fabric", "style name", "color", and each hat includes an image (picture url) of how the hat looks. There is a polling function that is called continously every 60 seconds and looks for any changes to the hat list. The list page has all the hats available to be viewed and can be accessed by clicking the link on the `HatsList` link on the mainpage, you can also access the list by clicking the link on the `HatsForm` link. You can access each individual hats details page on the both the top of the `HatsForm` and the `HatsList` page. The details that are shown are: "fabric", "style name", "color", "picture url", "location", "closet name", "shelf number", "section number", and "id". The value object is the location which shares the closet name, shelf number, id,  and section number with the hats. Using Insomnia you can request details, list data, you can create and delete any of the hats, I have included the APIs and endpoints below.

##### RESTful APIs

=======
Hats microservice is a website for creating and listing hats as part of a wardrobe service. Hats has 2 pages associated with the micrservice `HatsForm` and `HatsList` They can both be accessd from the main page listed @ `http://localhost:3000/`. The form page for hats allows you to create a new hat based on the properties of "fabric", "style name", "color", and each hat includes an image (picture url) of how the hat looks. There is a polling function that is called continously every 60 seconds and looks for any changes to the hat list. The list page has all the hats available to be viewed and can be accessed by clicking the link on the `HatsList` link on the mainpage, you can also access the list by clicking the link on the `HatsForm` link. You can access each individual hats details page on the both the top of the `HatsForm` and the `HatsList` page. The details that are shown are: "fabric", "style name", "color", "picture url", "location", "closet name", "shelf number", "section number", and "id". The value object is the location which shares the closet name, shelf number, id,  and section number with the hats. Using Insomnia you can request details, list data, you can create and delete any of the hats, I have included the APIs and endpoints below.

##### RESTful APIs

> > > > > > > 0579cd9 (update)

```http
Hats
```

| Action       | Request  |Endpoint  |
| :--------   | :------- | :-------------------------------- |
| Details hat| `GET` | `8090/api/hats/<int:pk>` |
| List hats | `GET` | `8090/api/hats/` |
| Create hat| `POST` |`8090/api/hats/` |
| Delete hat| `DELETE` | `8090/api/hats/<int:pk>` |

```http
Locations
```

| Action       | Request  |Endpoint  |
| :--------   | :------- | :-------------------------------- |
| Details location| `GET` | `8100/api/locations/<int:pk>` |
| List locations | `GET` | `8100/api/locations/` |
| Create location| `POST` |`8100/api/locations/` |
| Delete Location| `DELETE` | `8100/api/locations/<int:pk>` |

```http
Poller
```

Establishes a connection with the Hats Project.
The get_locations() retrieves location data from the RESTful API.
The get_locations() function sends a `GET` request to the API endpoint `http://wardrobe-api:8000/api/locations/`.
A JSON response is retrieved from the `GET` request.
The JSON response is parsed and the location information is extracted.
Poller.py updates or creates a corresponding Django model instance using the extracted information.
The poll() function runs in an infinite while loop.
Inside the loop, the get_locations() function is called to update the locations in the database.
If any exception occurs during the polling process, the exception is caught and logged.
The script waits for 60 seconds before starting the next iteration of the loop.
