from django.db import models
from django.urls import reverse


# Create your models here.
class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveBigIntegerField(null=True)
    shelf_number = models.PositiveBigIntegerField(null=True)
    import_href = models.CharField(
        max_length=200,
        null=True,
        unique=True,
    )

    def __str__(self):
        return f"{self.closet_name} {self.section_number} {self.shelf_number}"


class Hat(models.Model):
    fabric = models.CharField(max_length=50)
    style_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.PROTECT,
        null=True
    )

    def get_api_url(self):
        return reverse("api_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return f"|{self.style_name} - {self.fabric}/{self.color}|"

    class Meta:
        ordering = ("fabric", "style_name", "color")
