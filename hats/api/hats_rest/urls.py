from django.urls import path
from .views import api_detail_hat, api_list_hats

urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_detail_hat, name="api_detail_hat"),
]
