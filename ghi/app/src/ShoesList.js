import React from 'react';


function ShoesList(props) {

    const handleDelete = (id) => {
      fetch(`http://localhost:8080/api/shoes/${id}/`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then(response => {
          if (response.ok) {
            console.log('Item deleted successfully');
            window.location.reload()
          } else {
            console.error('Error deleting item');
          }
        })
        .catch(error => {
          console.error('Error deleting item', error);
        });
    };

  return (
      <table className="table table-striped table-bordered shadow p-4 mt-4">
        <thead>
          <tr className="text-center">
            <th>Shoe Model</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Image</th>
            <th>Closet Location</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            return (
              <tr key={shoe.id} className="text-center">
                <td>{ shoe.model }</td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.color }</td>
                <td><img src={ shoe.picture_url } className="thumbnail" width="12%"/></td>
                <td>{ shoe.bin.name }</td>
                <td><button onClick={() => {handleDelete(shoe.id)}}>Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoesList;
